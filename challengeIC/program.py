#!/usr/bin/env python3

import sys

def getInput(stream):
	perm = []
	nk   = []
	for i, line in enumerate(stream):
		line = line.split()
		if i % 2 == 0 or i == 0:
			nk = list(map(int, line))
		else:
			perm = list(map(int, line))
			getPerm(perm, nk[0], nk[1])


def getPerm(perm, n, k):
	for i in range(n):
		if k == 0:
			break

		maxVal = max(perm[i::])
		indMax = perm.index(maxVal)
		if perm[i] == maxVal:
			continue

		if perm[i] < maxVal:
			swap(perm, i, indMax)
			k -= 1
	print(' '.join(list(map(str, perm))))


def swap(perm, i, lastIndex):
	temp = perm[i]
	perm[i] = perm[lastIndex]
	perm[lastIndex] = temp


getInput(sys.stdin)
