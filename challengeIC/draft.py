#!/usr/bin/env python3

import sys

def getInput(stream):
	perm = []
	nk   = []
	for i, line in enumerate(stream):
		f = open("test.txt", "a+")
		f.write(line)
		f.close()
		line = line.split()
		if i % 2 == 0 or i == 0:
			nk = list(map(int, line))
		else:
			perm = list(map(int, line))
			#print(perm)
			getPerm(perm, nk[0], nk[1])


def getPerm(perm, n, k):
	check = 0
	while k != 0:
		maxVal = max(perm[check::])
		print(maxVal)
		if maxVal == perm[check]:
			check += 1
			continue
		tempPerm = list(map(str, perm))
		lastIndex = ''.join(tempPerm[check::]).rindex(str(maxVal))
		for i, element in enumerate(perm):
			if i > int(lastIndex):
				check = k
				break

			if element < maxVal:
				swap(perm, i, int(lastIndex))
				k -= 1
				break
	print(' '.join(list(map(str, perm))))

def swap(perm, i, lastIndex):
	temp = perm[i]
	perm[i] = perm[lastIndex]
	perm[lastIndex] = temp
	

getInput(sys.stdin)
