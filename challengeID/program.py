#!/usr/bin/env python3

# Contest I: Challenge ID: Triplets
# Programming Challenges
# Tucker Mercier

import sys
import itertools
#import numpy as np

# Functions

def find_triplets(num_list):

    num_list = sorted(num_list)
    triplets_list = []

    if len(num_list) < 3:
        return triplets_list

    for combination in itertools.combinations(num_list, 3):

        if combination in triplets_list:
            continue

        if combination[0] + combination[1] + combination[2] == 0:
            triplets_list.append(combination)

    return triplets_list



# Main Execution

if __name__ == '__main__':
    lines = sys.stdin.readlines()
    last_line = lines[-1]
    for line in lines:

        num_list = [int(x) for x in line.split()]

        triplets = find_triplets(num_list)

        for triplet in triplets:
            print(' + '.join(str(x) for x in triplet))

        if line is not last_line:
            print()
