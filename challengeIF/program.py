#!/usr/bin/env python3
#
# # Contest I: Challenge IF: Football Scores
# # Programming Challenges
# # Tucker Mercier, Thomas Lynch, Justin Virgadamo
#

'''
Rather than looking backwards, we can generate the table by looking forwards.
'''

import sys
from operator import itemgetter

POSSCORES = [7, 3, 2]
Scoring_List = []

def sum_list(slist):
    count = 0
    for term in slist:
        count += term
    return count

def add_it(fulllist, score, pos):
    #scoring_list = []
    for i in range(((score - sum_list(fulllist)) // POSSCORES[pos]) + 1):
        #print(i)
        templist = []
        for tempterm in range(i):
            templist.append(POSSCORES[pos])

        comb_list = sorted(fulllist + templist)
        if sum_list(comb_list) == score and comb_list not in Scoring_List:
            #print(' '.join(str(x) for x in sorted(fulllist + templist)))
            Scoring_List.append(comb_list)
            #print(Scoring_List)

        if pos < 2:
            add_it(fulllist + templist, score, pos + 1)


# Main Execution

if __name__ == '__main__':
    for line in sys.stdin:

        score = int(line)

        add_it([], score, 0)

        num_possibilities = len(Scoring_List)

        if num_possibilities == 1:
            print("There is 1 way to achieve a score of {}:".format(score))
        else:
            print("There are {} ways to achieve a score of {}:".format(num_possibilities, score))

        for s in sorted(Scoring_List):
            print(' '.join(str(x) for x in s))

        del Scoring_List[0:]
