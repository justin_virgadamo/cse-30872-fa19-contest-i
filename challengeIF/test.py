#!/usr/bin/env python3

'''
Rather than looking backwards, we can generate the table by looking forwards.
'''

import sys

POSSCORES = [7, 3, 2]

def sum_list(slist):
    count = 0
    for term in slist:
        count += term
    return count

def add_it(fulllist, score, pos):
    for i in range(((score - sum_list(fulllist)) // POSSCORES[pos]) + 1):
        #print(i)
        templist = []
        for tempterm in range(i):
            templist.append(POSSCORES[pos])

        if sum_list(fulllist + templist) == score:
            print(sorted(fulllist + templist))

        if pos < 2:
            add_it(fulllist + templist, score, pos + 1)

    

if __name__ == '__main__':
    add_it([], 12, 0)